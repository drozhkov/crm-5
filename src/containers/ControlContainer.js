import { connect } from 'react-redux';
import Control from '../components/Control/Control';
import { setEmployeeListSorting, setEmployeeListFilterByVacation } from '../actionCreators';
import { ID, NAME_TITLE, ID_TITLE } from '../constants';

const setSortingTitle = sortBy => {
  if (sortBy === ID) return NAME_TITLE;
  return ID_TITLE;
};

const mapStateToProps = state => ({
  sortBy: state.sortBy,
  sortTitle: setSortingTitle(state.sortBy),
  filterByVacation: state.filterByVacation
});

const mapDispatchToProps = dispatch => ({
  setSort: sortBy => dispatch(setEmployeeListSorting(sortBy)),
  setFilterByVacation: filterBy => dispatch(setEmployeeListFilterByVacation(filterBy))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Control);
