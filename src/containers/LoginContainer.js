import { connect } from 'react-redux';
import Login from '../components/Login/Login';
import { signInAction } from '../actionCreators';

const mapDispatchToProps = dispatch => ({
  signIn: (values, actions) => dispatch(signInAction(values, actions))
});

export default connect(
  null,
  mapDispatchToProps
)(Login);
