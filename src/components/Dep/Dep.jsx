import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import style from './Dep.css';

class Dep extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
    this.openDescendants = this.openDescendants.bind(this);
    this.setFilter = this.setFilter.bind(this);
  }

  setFilter() {
    this.props.clickHandler(this.props.id);
  }

  openDescendants() {
    this.setState(state => ({ open: !state.open }));
  }

  render() {
    const classes = cx({
      [style.dep]: true,
      [style.dep_open]: this.state.open,
      [style.dep_active]: this.props.id === this.props.filterByDepartment
    });
    const moreButton = this.props.hasDescendants ? (
      <button type="button" className={style.dep__more} onClick={this.openDescendants}>
        <span />
        <span />
      </button>
    ) : (
      ''
    );

    return (
      <div className={classes}>
        {moreButton}
        <button
          type="button"
          data-id={this.props.id}
          className={style.dep__btn}
          onClick={this.setFilter}
        >
          {this.props.title}
        </button>
      </div>
    );
  }
}

Dep.defaultProps = {
  hasDescendants: undefined
};

Dep.propTypes = {
  clickHandler: PropTypes.func.isRequired,
  id: PropTypes.number.isRequired,
  filterByDepartment: PropTypes.number.isRequired,
  hasDescendants: PropTypes.bool,
  title: PropTypes.string.isRequired
};

export default Dep;
