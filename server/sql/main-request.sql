SELECT d.id, d.title,
GROUP_CONCAT(dc.descendant_id ORDER BY dc.descendant_id ASC) AS descendants,
GROUP_CONCAT(CASE WHEN dc.depth = 1 THEN dc.descendant_id ELSE NULL END ORDER BY dc.descendant_id ASC) AS directDescendants
FROM departments d
LEFT JOIN departments_closure dc ON dc.ancestor_id = d.id
GROUP BY id
